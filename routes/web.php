<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', 'AuthController@index')->name('login');
Route::post('post-login', 'AuthController@postLogin')->name('login.post');
Route::get('dashboard', 'AuthController@dashboard')->name('dashboard'); 
Route::get('logout', 'AuthController@logout')->name('logout');

Route::get('/registration', 'AuthController@registration')->name('registration');
Route::post('post-registration', 'AuthController@postRegistration')->name('regist.post'); 

Route::post('post-create', 'AuthController@postCreateUser')->name('create.post');

Route::get('edit','AuthController@showUserById')->name('show.user.id');
Route::post('update-data','AuthController@updateDataUser')->name('update.data.user');

Route::delete('delete-data','AuthController@deleteDataUser')->name('delete.data.user');
