<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  
    <title>Waizly</title>
</head>
<body>
    <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
              <div class="card bg-white">
                <div class="card-body p-5">
                  <form class="mb-3 mt-md-4" id="registUser">
                    @csrf
                    <h2 class="fw-bold mb-2">Registration Waizly!</h2>
                    <p class=" mb-5">Please create your account!</p>
                    <div class="mb-3">
                      <label for="email" class="form-label ">Email</label>
                      <input type="email" class="form-control" id="email" placeholder="email" name="email">
                      @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                    <div class="mb-3">
                      <label for="password" class="form-label ">Password</label>
                      <input type="password" class="form-control" id="password" placeholder="*******" name="password">
                      @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                      @endif
                    </div>

                    <div class="d-grid">
                      <button class="btn btn-outline-dark" type="submit">Sign Up</button>
                    </div>
                  </form>
                  <div>
                    <p class="mb-0  text-center">Have account? <a href="{{ url('/'); }}" class="text-primary fw-bold">Login</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</body>

<script type="text/javascript">
  $('#registUser').submit(function(e) {
    e.preventDefault();
    var email =  $('#email').val();
    var password =  $('#password').val();

    $.ajax({
        type: 'POST',
        url: '{{ route('regist.post') }}',
        data: { 'email': email, 'password': password, '_token':'{{ csrf_token()}}'},
        dataType: 'json',
        success: function(data) {
            Swal.fire(
              'Success Register',
              'Silahkan login kembali!',
              'success',
            ).then(function(){
              window.location.href = "{{ route('login')}}";
            });
        },
        error:function(data){
            var error = data.responseJSON;
            if ('email' in error.errors && 'password' in error.errors) {
                Swal.fire(
                  'Error',
                  error.errors.email[0] + '<br>' + error.errors.password[0],
                  'error'
                )
            } else if ('password' in error.errors) {
                Swal.fire(
                  'Error',
                  error.errors.password[0],
                  'error'
                )
            } else {
                Swal.fire(
                  'Error',
                  error.errors.email[0],
                  'error'
                )
            }
        }
    });
  });
</script>

</html>