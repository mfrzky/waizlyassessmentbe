<!DOCTYPE html>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <title>Waizly</title>
</head>
<body>
    <nav class="navbar navbar-light navbar-expand-lg mb-5" style="background-color: #e3f2fd;">
        <div class="container">
            <h1 class="font-italic">Waizly</h1>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="float-right" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <button class="btn nav-link fw-bold confirmLogout">Logout</button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <button class="btn btn-info fw-bold" data-toggle="modal" data-target="#modalAddUser">
                + Add User
            </button>

            <span class="h5">
                HI, {{$userLogin ? $userLogin['email'] : 'User!'}}
            </span>
        </div>

        <table class="table table-hover table-bordered text-center">
            <thead>
                <tr>
                    <td>Email</td>
                    <td>Created At</td>
                    <td>Aksi</td>
                </tr>
            </thead>
            <tbody>
                @if($allUser)
                    @foreach($allUser as $user)
                        <tr>
                            <td>{{ $user['email'] }}</td>
                            <td>{{ $user['created_at'] }}</td>
                            <td>
                                <button id="user-{{$user['id']}}" class="btn btn-warning text-white fw-bold edit" data-id="{{$user['id']}}" data-toggle="modal" data-target="#modalEditUser">
                                    <i class="fa-solid fa-pencil"></i>
                                </button>
                                <button id="user-{{$user['id']}}" class="btn btn-info text-white fw-bold show" data-id="{{$user['id']}}" data-toggle="modal" data-target="#modalShowUser">
                                    <i class="fa-solid fa-eye"></i>
                                </button>
                                <button id="user-{{$user['id']}}" class="btn btn-danger text-white fw-bold confirmDelete" data-id="{{$user['id']}}">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>Data Kosong!</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="modalAddUser" tabindex="-1" aria-labelledby="modalAddUser" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add User</h5>
                    <button type="button" class="close" id="closeAddUserModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addUser">
                        @csrf
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="email" name="email">
                            
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="*******" name="password">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalShowUser" tabindex="-1" aria-labelledby="modalShowUser" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">User</h5>
                    <button type="button" class="close" id="closeShowUserModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Email</label>
                        <p id="emailShow" placeholder="email" name="emailShow"></p>
                    </div>
                    <button type="submit" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditUser" tabindex="-1" aria-labelledby="modalEditUser" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit User</h5>
                    <button type="button" class="close" id="closeEditUserModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editUser">
                        @csrf
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" id="emailEdit" placeholder="email" name="emailEdit">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @yield('content')
</body>

<script type="text/javascript">
    $(document).on('ajaxComplete ready', function () {
        $('.modalMd').off('click').on('click', function () {
            $('#modalMdContent').load($(this).attr('value'));
            $('#modalMdTitle').html($(this).attr('title'));
        });
    });

    $('#addUser').submit(function(e) {
        e.preventDefault();
        var email =  $('#email').val();
        var password =  $('#password').val();

        $.ajax({
            type: 'POST',
            url: '{{ route('create.post') }}',
            data: { 'email': email, 'password': password, '_token':'{{ csrf_token()}}'},
            dataType: 'json',
            success: function(data) {
                Swal.fire(
                    'Success',
                    'Sukses Membuat User!',
                    'success',
                ).then(function(){
                    window.location.reload();
                });
            },
            error:function(data){
                var error = data.responseJSON;
                if ('email' in error.errors && 'password' in error.errors) {
                    Swal.fire(
                        'Error',
                        error.errors.email[0] + '<br>' + error.errors.password[0],
                        'error'
                    )
                } else if ('password' in error.errors) {
                    Swal.fire(
                        'Error',
                        error.errors.password[0],
                        'error'
                    )
                } else {
                    Swal.fire(
                        'Error',
                        error.errors.email[0],
                        'error'
                    )
                }
            }
        });
    });

    var userId
    $('.edit').on("click",function() {
        userId = $(this).attr('data-id');       
        $.ajax({
            url : "{{route('show.user.id')}}?id=" + $(this).attr('data-id'),
            type: "GET",
            dataType: "JSON",
            success: function(data){
                $('#emailEdit').val(data.email);
            }
        });
    });

    $('.show').on("click",function() {
        userId = $(this).attr('data-id');       
        $.ajax({
            url : "{{route('show.user.id')}}?id=" + $(this).attr('data-id'),
            type: "GET",
            dataType: "JSON",
            success: function(data){
                $('#emailShow').text(data.email);
            }
        });
    });

    $('#editUser').submit(function(e) {
        e.preventDefault();
        var email =  $('#emailEdit').val();

        $.ajax({
            type: 'POST',
            url: '{{ route('update.data.user') }}',
            data: { 'id': userId, 'email': email, '_token':'{{ csrf_token()}}'},
            dataType: 'json',
            success: function(data) {
                Swal.fire(
                    'Success',
                    'Sukses Update User!',
                    'success',
                ).then(function(){
                    window.location.reload();
                });
            },
            error: function(data){
                var error = data.responseJSON;
                if (error) {
                    Swal.fire(
                        'Error',
                        'Silahkan cek kembali!',
                        'error'
                    )
                }
            }
        });
    });

    $('.confirmDelete').click(function(e) {
        swal.fire({
            title: 'Apakah Anda yakin ingin menghapus user ini?',
            text: "Anda tidak bisa mengembalikannya",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((check) => {
            if (check.isConfirmed === true) {
                $.ajax({
                    type: 'DELETE',
                    url : "{{route('delete.data.user')}}?id=" + $(this).attr('data-id'),
                    data: { 'id': $(this).attr('data-id'), '_token':'{{ csrf_token()}}'},
                    dataType: 'json',
                    success: function(data) {
                        Swal.fire(
                            'Success',
                            'Sukses Delete User!',
                            'success',
                        ).then(function(){
                            window.location.reload();
                        });
                    },
                    error: function(data){
                        var error = data.responseJSON;
                        if (error) {
                            Swal.fire(
                                'Error',
                                'Silahkan cek kembali!',
                                'error'
                            )
                        }
                    }
                })
            }
        })
    });

    $('.confirmLogout').click(function(e) {
        swal.fire({
            title: 'Yakin ingin log out?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((check) => {
            if (check.isConfirmed === true) {
                $.ajax({
                    type: 'GET',
                    url : "{{ route('logout') }}",
                    success: function() {
                        window.location.href = "{{ route('login')}}";
                    }
                })
            }
        })
    });

    $(document).ready(function() {
        $('#closeAddUserModal').click(function() {
            $('#email').val('');
            $('#password').val('');
        })
    })

</script>
</html>