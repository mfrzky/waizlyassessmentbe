<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Hash;
use Session;
use DB;

class AuthController extends Controller
{
    public function index(){
        if (Auth::check()) {
            return redirect('dashboard');
        }else{
            return view('login.login');
        }
    }
    
    public function dashboard(){
        if(Auth::check()){
            $getUser = User::all();
            return view('dashboard.home',[
                'allUser' => $getUser,
                'userLogin' => Auth::user()
            ]);
        }
  
        return Response::json(array('success' => true), 200);
    }

    public function postLogin(Request $request){
        $tableUser = User::all();

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $credentials = [
            'email' => $request['email'],
            'password' => $request['password'],
        ];

        if(Auth::attempt($credentials)) {
            return Response::json(array('success' => true), 200);
        } else {
            return Response::json(array('error' => true), 400);
        }
    }

    public function registration(){
        return view('login.register');
    }

    public function postRegistration(Request $request){  
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
           
        User::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password'])
        ]);
         
        return Response::json(array('success' => true), 200);
    }

    public function postCreateUser(Request $request){
        $rules = array(
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }

        User::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password'])
        ]);

        return Response::json(array('success' => true), 200);
    } 

    public function showUserById(Request $request){
        $data = User::findOrFail($request->get('id'));
        echo json_encode($data);
    }

    public function updateDataUser(Request $request){
        $post           = User::find($request['id']);
        $post->email    = $request['email'];
        $post->save();
        
        return Response::json(array('success' => true), 200);
    }

    public function deleteDataUser(Request $request){
        $deletUser = User::find($request['id']); 
        $deletUser->delete();

        return Response::json(array('success' => true), 200);
    }

    public function logout(){
        Session::flush();
        Auth::logout();
  
        return Redirect('/');
    }
}
